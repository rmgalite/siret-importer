# Base image
FROM ubuntu:trusty

# Install sqlite3
RUN apt-get update && apt-get install build-essential gnupg2 curl sqlite3 unzip -y

# Install rvm
# RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
# RUN \curl -sSL https://get.rvm.io | bash -s stable
# RUN usermod -aG rvm $(whoami)
SHELL [ "/bin/bash", "-l", "-c" ]
# RUN source /etc/profile.d/rvm.sh
# RUN rvm install 2.5.1
# RUN gem install bundler

# Setup the working directory
ADD . /app/
WORKDIR /app
# RUN bundle
# bundle install

# ENTRYPOINT [ "/bin/bash", "-l", "-c" ]