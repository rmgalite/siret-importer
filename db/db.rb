require 'active_record'
require 'sqlite3'

configuration = YAML::load(IO.read(
  File.expand_path(File.dirname(__FILE__)) + '/database.yml'
))

ActiveRecord::Base.establish_connection(configuration)

class Establishment < ActiveRecord::Base
  belongs_to :owner, foreign_key: :siren
end

class Owner < ActiveRecord::Base
  has_many :establishments, foreign_key: :siren
end
