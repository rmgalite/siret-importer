import sqlite3
from algoliasearch import algoliasearch
from functools import reduce
import progressbar

# Init algolia client

client = algoliasearch.Client('F6ZK2OJ8P7', 'bcdde2c8fdcb6273baa20069882a37cf')
index = client.init_index('mael_sirets')

# Build up the SQL query

conn = sqlite3.connect('db.sqlite3')
conn.row_factory = sqlite3.Row
cursor = conn.cursor()

query = '''
  SELECT COUNT(*) FROM owners
  LIMIT 0, 100000
'''
count = cursor.execute(query).fetchone()[0]

print("%s establishments found." % count)

query = '''
  SELECT * FROM owners
  LEFT JOIN establishments on establishments.siren = owners.siren
  LIMIT 0, 100000
'''

cursor.execute(query)


def build_records_from_rows(row):
    def build_from_row(acc, key):
        acc[key] = row[key]
        acc['objectID'] = row['siret']
        return acc

    return reduce(build_from_row, row.keys(), {})


with progressbar.ProgressBar(max_value=count) as bar:
    current_count = 0

    while True:
        rows = cursor.fetchmany(10000)

        if not rows:
            break

        index.add_objects(map(build_records_from_rows, rows))
        current_count += 10000
        bar.update(current_count)
