#!/bin/bash

set -e

echo "Downloading owners csv zip"
curl 'http://files.data.gouv.fr/insee-sirene/StockUniteLegale_utf8.zip' --output files/owners.zip

echo "Extracting owners csv file"
unzip files/owners.zip -d files
mv files/$(unzip -Z1 files/owners.zip) files/owners.csv
rm files/owners.zip

echo "Downloading establishments csv zip"
curl 'http://files.data.gouv.fr/insee-sirene/StockEtablissement_utf8.zip' --output files/establishments.zip

echo "Extracting establishments csv file"
unzip files/establishments.zip -d files
mv files/$(unzip -Z1 files/establishments.zip) files/establishments.csv
rm files/establishments.zip

echo "Importing data to a SQLite database"
sqlite3 db.sqlite3 < structure.sql
sqlite3 db.sqlite3 <<EOF
.mode csv
.import files/owners.csv owners
CREATE UNIQUE INDEX "index_owners_on_siren" ON "owners" ("siren");
.print Owners imported in db.
.import files/establishments.csv establishments
CREATE INDEX "index_establishments_on_nic" ON "establishments" ("nic");
CREATE UNIQUE INDEX "index_establishments_on_siret" ON "establishments" ("siret");
.print Establishments imported in db.
.exit
EOF

rm files/establishments.csv files/owners.csv
